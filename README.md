This repository contains the source for the project "Object Clustering Using NAO Robot" for GNU/Linux and Mac OS X. This is my Bachelor Diploma project. Both on Mac and Linux OpenCV 2.4.9 or later is required. On Linux NAO C++ SDK should be installed.

This project makes NAO robot group the objects in front of him. On Mac the functionality is limited to reading two images from filesystem, detecting the objects from them and showing which object is part of which group. On Linux, the program connects to the robot and remotely uses some of his modules, to acquire the images from the camera, move, speak and grasp the objects.


Here is a slightly simplified version of the interaction between objects and flow of messages:

![maxhameak3.png](https://bitbucket.org/repo/nRoGER/images/3719620742-maxhameak3.png)


And here are the dependencies and relationships between classes:

![21.jpg](https://bitbucket.org/repo/nRoGER/images/936427485-21.jpg)

![22.jpg](https://bitbucket.org/repo/nRoGER/images/3332886412-22.jpg)

![23.jpg](https://bitbucket.org/repo/nRoGER/images/1607912675-23.jpg)

![24.jpg](https://bitbucket.org/repo/nRoGER/images/3549801841-24.jpg)

![25.jpg](https://bitbucket.org/repo/nRoGER/images/3223579717-25.jpg)

![26.jpg](https://bitbucket.org/repo/nRoGER/images/2528544697-26.jpg)

![27.jpg](https://bitbucket.org/repo/nRoGER/images/3433538344-27.jpg)